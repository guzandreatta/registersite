﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using MVCIdentityConfirm.Models;
using System.IO;

namespace MVCIdentityConfirm.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.Email, model.Password);
                if (user != null)
                {
                    if (user.ConfirmedEmail == true)
                    {
                        await SignInAsync(user, model.RememberMe);
                        TempData["userId"] = user.Id;
                        return RedirectToAction("UpdateProfile");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Confirm Email Address.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                RegisterViewModel rvm = new RegisterViewModel();
                rvm.Email = email;
                return View(rvm);
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.Email };
                user.Email = model.Email;
                user.ConfirmedEmail = true;
                user.ForgottenPassword = false;
                user.Name = model.Name;
                user.LastName = model.LastName;
                user.Country = model.Country;
                if (!model.FoodChoiceStandard && !model.FoodChoiceHalal && !model.FoodChoiceCeliac && !model.FoodChoiceVegetarian && !model.FoodChoiceKosher && !model.FoodChoiceVegan)
                {
                    ModelState.AddModelError("FoodChoice", "You must select at least one. If indifferent just check Standard");
                    return View(model);
                }

                user.FoodChoiceStandard = model.FoodChoiceStandard;
                user.FoodChoiceHalal = model.FoodChoiceHalal;
                user.FoodChoiceCeliac = model.FoodChoiceCeliac;
                user.FoodChoiceVegetarian = model.FoodChoiceVegetarian;
                user.FoodChoiceKosher = model.FoodChoiceKosher;
                user.FoodChoiceVegan = model.FoodChoiceVegan;

                user.FoodAllergiesEggs = model.FoodAllergiesEggs;
                user.FoodAllergiesWheat = model.FoodAllergiesWheat;
                user.FoodAllergiesLactose = model.FoodAllergiesLactose;
                user.FoodAllergiesGluten = model.FoodAllergiesGluten;
                user.FoodAllergiesFish = model.FoodAllergiesFish;
                user.FoodAllergiesNuts = model.FoodAllergiesNuts;

                user.FoodAllergiesOther = model.FoodAllergiesOther;

                user.Additional = model.Additional;
                user.LastUpdated = DateTime.Now;

                var result = await UserManager.CreateAsync(user, model.Password);

                model.Id = user.Id;

                if (result.Succeeded)
                {
                    System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                       new System.Net.Mail.MailAddress("events@meetyama.com", "Web Registration"),
                       new System.Net.Mail.MailAddress(user.Email));
                    m.Subject = "You are now registred to the Repatha Brand Summit";
                    m.Body = RenderRazorViewToString("EmailConfirmation", model);
                    m.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Send(m);
                    TempData["ResendError"] = "";
                    TempData["ResendOk"] = "";
                    return RedirectToAction("Confirm", "Account", new { Email = user.Email });

                    //System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                    //    new System.Net.Mail.MailAddress("fabian@yamagroup.com", "Web Registration"),
                    //    new System.Net.Mail.MailAddress(user.Email));
                    //m.Subject = "Email confirmation";
                    //m.Body = RenderRazorViewToString("EmailConfirmation", model);
                    //// m.Body = string.Format("Dear {0}<BR/>Thank you for your registration, please click on the below link to complete your registration: <a href=\"{1}\" title=\"User Email Confirm\">{1}</a>", user.UserName, Url.Action("ConfirmEmail", "Account", new { Token = user.Id, Email = user.Email }, Request.Url.Scheme));
                    //m.IsBodyHtml = true;
                    //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                    ////smtp.Host = 587;
                    //smtp.Credentials = new System.Net.NetworkCredential("fabian@yamagroup.com", "Yama.123");
                    //smtp.EnableSsl = true;
                    //smtp.Send(m);
                    //TempData["ResendError"] = "";
                    //TempData["ResendOk"] = "";
                    //return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Resend(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                TempData["Resend"] = "We were unable to find the email address in our database. Please register";
                return RedirectToAction("Confirm", "Account", new { Email = email });
            }
            else
            {
                var user = UserManager.FindByName(email);

                RegisterViewModel model = new RegisterViewModel();

                model.Name = user.Name;
                model.Id = user.Id;
                model.LastName = user.LastName;
                model.Country = user.Country;

                model.FoodChoiceStandard = user.FoodChoiceStandard;
                model.FoodChoiceHalal = user.FoodChoiceHalal;
                model.FoodChoiceCeliac = user.FoodChoiceCeliac;
                model.FoodChoiceVegetarian = user.FoodChoiceVegetarian;
                model.FoodChoiceKosher = user.FoodChoiceKosher;
                model.FoodChoiceVegan = user.FoodChoiceVegan;

                model.FoodAllergiesEggs = user.FoodAllergiesEggs;
                model.FoodAllergiesWheat = user.FoodAllergiesWheat;
                model.FoodAllergiesLactose = user.FoodAllergiesLactose;
                model.FoodAllergiesGluten = user.FoodAllergiesGluten;
                model.FoodAllergiesFish = user.FoodAllergiesFish;
                model.FoodAllergiesNuts = user.FoodAllergiesNuts;
                model.FoodAllergiesOther = user.FoodAllergiesOther;

                System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                           new System.Net.Mail.MailAddress("fabian@yamagroup.com", "Web Registration"),
                           new System.Net.Mail.MailAddress(email));
                m.Subject = "Email confirmation";
                m.Body = RenderRazorViewToString("EmailConfirmation", model);
                // m.Body = string.Format("Dear {0}<BR/>Thank you for your registration, please click on the below link to complete your registration: <a href=\"{1}\" title=\"User Email Confirm\">{1}</a>", user.UserName, Url.Action("ConfirmEmail", "Account", new { Token = user.Id, Email = user.Email }, Request.Url.Scheme));
                m.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                //smtp.Host = 587;
                smtp.Credentials = new System.Net.NetworkCredential("fabian@yamagroup.com", "Yama.123");
                smtp.EnableSsl = true;
                smtp.Send(m);
                TempData["Resend"] = "We just sent a new confirmation email, consider checking your spam folder. If you are still not getting the email please contact XXXXXXX";
                return RedirectToAction("Confirm", "Account", new { Email = user.Email });
            }

        }

        [AllowAnonymous]
        public ActionResult Confirm(string Email)
        {
            ViewBag.Email = Email;
            return View();
        }
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string Token, string Email)
        {
            ApplicationUser user = this.UserManager.FindById(Token);
            if (user != null)
            {
                if (user.Email == Email)
                {
                    user.ConfirmedEmail = true;
                    await UserManager.UpdateAsync(user);
                    await SignInAsync(user, isPersistent: false);
                    //if email confirmed ok:
                    TempData["userId"] = user.Id;
                    return RedirectToAction("UpdateProfile", "Account");
                }
                else
                {
                    return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                }
            }
            else
            {
                return RedirectToAction("Confirm", "Account", new { Email = "" });
            }

        }

        //
        // POST: /Account/Disassociate
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        //{
        //    ManageMessageId? message = null;
        //    IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded)
        //    {
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    }
        //    else
        //    {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction("Manage", new { Message = message });
        //}

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        ////
        //// GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var user = await UserManager.FindAsync(loginInfo.Login);
        //    if (user != null)
        //    {
        //        await SignInAsync(user, isPersistent: false);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    else
        //    {
        //        // If the user does not have an account, then prompt the user to create an account
        //        ViewBag.ReturnUrl = returnUrl;
        //        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
        //    }
        //}

        ////
        //// POST: /Account/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Request a redirect to the external login provider to link a login for the current user
        //    return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        //}

        ////
        //// GET: /Account/LinkLoginCallback
        //public async Task<ActionResult> LinkLoginCallback()
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //    }
        //    var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
        //    if (result.Succeeded)
        //    {
        //        return RedirectToAction("Manage");
        //    }
        //    return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //}

        ////
        //// POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new ApplicationUser() { UserName = model.UserName };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInAsync(user, isPersistent: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        //[ChildActionOnly]
        //public ActionResult RemoveAccountList()
        //{
        //    var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
        //    ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
        //    return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        //}

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View();
        }

        //this method sends the email with the link for the password reset
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ForgotViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);
                if (user == null)
                {
                    //redirect to view and alert that this mail address was not found
                    ModelState.AddModelError("Email", "This e-mail address was not found in out database, please check it.");
                    return View(model);
                }
                else
                {
                    user.ForgottenPassword = true;

                    System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                       new System.Net.Mail.MailAddress("events@meetyama.com", "Password Reset"),
                       new System.Net.Mail.MailAddress(user.Email));
                    m.Subject = "You requested a password reset.";
                    m.Body = string.Format("Hey {0} <BR/><BR/> <a href=\"{1}\" title=\"Reset password\"> Click the link to complete the password reset process </a>", model.Email, Url.Action("ResetPasswordSecondStep", "Account", new { Token = user.Id, Email = model.Email }, Request.Url.Scheme));
                    m.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Send(m);
                    TempData["ResendError"] = "";
                    TempData["ResendOk"] = "";

                    return RedirectToAction("ResetPasswordEmailSent", "Account", new { Email = model.Email });
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordSecondStep(string token, string email)
        {
            //check if the recieved email address has a state of forgotten password and a valid token
            var user = UserManager.FindByName(email);
            if (user.Id == token)
            {
                //valid password reset. Show new password screen
                return View();
            }

            //user.ForgottenPassword = false;
            return null;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPasswordThirdStep(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);
                user.ForgottenPassword = false;
                UserManager.RemovePassword(user.Id);
                //set the new password
                IdentityResult result = UserManager.AddPassword(user.Id, model.NewPassword);
                if (result.Succeeded)
                {
                    return RedirectToAction("UpdateProfile", "Account", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                else
                {
                    AddErrors(result);
                }
            }
            return null;
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordEmailSent()
        {
            return View();
        }

        public ActionResult UpdateProfile()
        {
            if (TempData["userId"] != null)
            {
                ApplicationUser user = this.UserManager.FindById(TempData["userId"].ToString());
                UpdateProfileViewModel model = new UpdateProfileViewModel();
                model.Email = user.Email;
                model.Name = user.Name;
                model.LastName = user.LastName;
                model.Country = user.Country;
                model.FoodChoiceStandard = user.FoodChoiceStandard;
                model.FoodChoiceHalal = user.FoodChoiceHalal;
                model.FoodChoiceCeliac = user.FoodChoiceCeliac;
                model.FoodChoiceVegetarian = user.FoodChoiceVegetarian;
                model.FoodChoiceKosher = user.FoodChoiceKosher;
                model.FoodChoiceVegan = user.FoodChoiceVegan;
                model.FoodAllergiesEggs = user.FoodAllergiesEggs;
                model.FoodAllergiesWheat = user.FoodAllergiesWheat;
                model.FoodAllergiesLactose = user.FoodAllergiesLactose;
                model.FoodAllergiesGluten = user.FoodAllergiesGluten;
                model.FoodAllergiesFish = user.FoodAllergiesFish;
                model.FoodAllergiesNuts = user.FoodAllergiesNuts;
                model.FoodAllergiesOther = user.FoodAllergiesOther;
                model.Additional = user.Additional;
                return View(model);
            }
            else
            {
                TempData["RegistrationCompleted"] = "Thanks for completing your registration. See you there!";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(UpdateProfileViewModel model)
        {
            if (ModelState.IsValid)
            {

                var user = UserManager.FindByName(model.Email);
                user.Name = model.Name;
                user.LastName = model.LastName;
                user.Country = model.Country;

                if (!model.FoodChoiceStandard && !model.FoodChoiceHalal && !model.FoodChoiceCeliac && !model.FoodChoiceVegetarian && !model.FoodChoiceKosher && !model.FoodChoiceVegan)
                {
                    ModelState.AddModelError("FoodChoice", "You must select at least one. If indifferent just check Standard");
                    return View(model);
                }

                user.FoodChoiceStandard = model.FoodChoiceStandard;
                user.FoodChoiceHalal = model.FoodChoiceHalal;
                user.FoodChoiceCeliac = model.FoodChoiceCeliac;
                user.FoodChoiceVegetarian = model.FoodChoiceVegetarian;
                user.FoodChoiceKosher = model.FoodChoiceKosher;
                user.FoodChoiceVegan = model.FoodChoiceVegan;

                user.FoodAllergiesEggs = model.FoodAllergiesEggs;
                user.FoodAllergiesWheat = model.FoodAllergiesWheat;
                user.FoodAllergiesLactose = model.FoodAllergiesLactose;
                user.FoodAllergiesGluten = model.FoodAllergiesGluten;
                user.FoodAllergiesFish = model.FoodAllergiesFish;
                user.FoodAllergiesNuts = model.FoodAllergiesNuts;
                user.FoodAllergiesOther = model.FoodAllergiesOther;

                user.Additional = model.Additional;
                user.LastUpdated = DateTime.Now;
                UserManager.UpdateAsync(user);

                //send email???
                TempData["RegistrationCompleted"] = "yes";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Please check the data");
                return View(model);
            }
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }


        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.Contains("Name") && error.Contains("is already taken"))
                {
                    ModelState.AddModelError("Email", "This email is already taken, did you <a href=\"" + Url.Action("ResetPassword", "Account", Request.Url.Scheme) + "\" title=\"forget your password?\">forget your password?</a>");
                }
                else
                {
                    ModelState.AddModelError("", error);
                }
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }




        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}