﻿using MVCIdentityConfirm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCIdentityConfirm.Controllers
{
    public class BackendController : Controller
    {
        // GET: Backend
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var username = User.Identity.Name.ToString().Split('@')[1];
                    if (username.Equals("yamagroup.com"))
                    {
                        ApplicationDbContext db = new ApplicationDbContext();
                        var list = db.Users.ToList();
                        db.Dispose();
                        return View(list);
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
    }
}