﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;

namespace MVCIdentityConfirm.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Key]
        public string Email { get; set; }
        public bool ConfirmedEmail { get; set; }

        //new fields
        public bool ForgottenPassword { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public bool FoodChoiceStandard { get; set; }
        public bool FoodChoiceHalal { get; set; }
        public bool FoodChoiceCeliac { get; set; }
        public bool FoodChoiceVegetarian { get; set; }
        public bool FoodChoiceKosher { get; set; }
        public bool FoodChoiceVegan { get; set; }

        public bool FoodAllergiesEggs { get; set; }
        public bool FoodAllergiesWheat { get; set; }
        public bool FoodAllergiesLactose { get; set; }
        public bool FoodAllergiesGluten { get; set; }
        public bool FoodAllergiesFish { get; set; }
        public bool FoodAllergiesNuts { get; set; }
        public string FoodAllergiesOther { get; set; }
        public string Additional { get; set; }

        public string FoodChoiceText
        {
            get
            {
                string ret = "";
                if (FoodChoiceStandard)
                {
                    ret += "Standard,";
                }
                if (FoodChoiceHalal)
                {
                    ret += "Halal,";
                }
                if (FoodChoiceCeliac)
                {
                    ret += "Celiac,";
                }
                if (FoodChoiceVegetarian)
                {
                    ret += "Vegetarian,";
                }
                if (FoodChoiceKosher)
                {
                    ret += "Kosher,";
                }
                if (FoodChoiceVegan)
                {
                    ret += "Vegan,";
                }

                return ret.Remove(ret.Length - 1);
            }
        }
        public string FoodAllergiesText {
            get
            {
                string ret = "";
                if (FoodAllergiesEggs)
                {
                    ret += "Eggs,";
                }
                if (FoodAllergiesWheat)
                {
                    ret += "Wheat,";
                }
                if (FoodAllergiesLactose)
                {
                    ret += "Lactose,";
                }
                if (FoodAllergiesGluten)
                {
                    ret += "Gluten,";
                }
                if (FoodAllergiesFish)
                {
                    ret += "Fish,";
                }
                if (FoodAllergiesNuts)
                {
                    ret += "Nuts,";
                }
                if (!string.IsNullOrEmpty(FoodAllergiesOther))
                {
                    ret += FoodAllergiesOther + ",";
                }
                return ret.Remove(ret.Length - 1);
            }
        }
        public DateTime LastUpdated { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}