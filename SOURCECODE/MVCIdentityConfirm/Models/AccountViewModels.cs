﻿using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace MVCIdentityConfirm.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterViewModel
    {
        public string Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The first name is a required field.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The last name is a required field.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Please select a country from the list.")]
        [Display(Name = "Country")]
        public string Country { get; set; } 

        public bool FoodChoiceStandard { get; set; }
        public bool FoodChoiceHalal { get; set; }
        public bool FoodChoiceCeliac { get; set; }
        public bool FoodChoiceVegetarian { get; set; }
        public bool FoodChoiceKosher { get; set; }
        public bool FoodChoiceVegan { get; set; }

        public bool FoodAllergiesEggs    { get; set; }
        public bool FoodAllergiesWheat   { get; set; }
        public bool FoodAllergiesLactose { get; set; }
        public bool FoodAllergiesGluten  { get; set; }
        public bool FoodAllergiesFish    { get; set; }
        public bool FoodAllergiesNuts { get; set; }

        [StringLength(200)]
        [Display(Name = "Please specify")]
        public string FoodAllergiesOther { get; set; }

        [StringLength(1000)]
        [Display(Name = "Additional Requirements/Comments")]
        public string Additional { get; set; }

    }

    public class UpdateProfileViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The first name is a required field.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The last name is a required field.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Please select a country from the list.")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        public bool FoodChoiceStandard { get; set; }
        public bool FoodChoiceHalal { get; set; }
        public bool FoodChoiceCeliac { get; set; }
        public bool FoodChoiceVegetarian { get; set; }
        public bool FoodChoiceKosher { get; set; }
        public bool FoodChoiceVegan { get; set; }

        public bool FoodAllergiesEggs { get; set; }
        public bool FoodAllergiesWheat { get; set; }
        public bool FoodAllergiesLactose { get; set; }
        public bool FoodAllergiesGluten { get; set; }
        public bool FoodAllergiesFish { get; set; }
        public bool FoodAllergiesNuts { get; set; }

        [StringLength(200)]
        [Display(Name = "Please specify")]
        public string FoodAllergiesOther { get; set; }

        [StringLength(1000)]
        [Display(Name = "Additional Requirements/Comments")]
        public string Additional { get; set; }

    }

}
