﻿$(document).ready(function () {
    $('#usersList').DataTable();
});

function showLoading() {
    $(".loading").show();
}

function sub() {
    if ($("#registerForm").valid() == true) {
        showLoading();
        $("#registerForm").submit();
    }
}

$("#Other").change(function () {
    $("#FoodAllergiesOther").toggleClass("hidden");
});


$(".show-loading").on("click", function () {
    showLoading();
});